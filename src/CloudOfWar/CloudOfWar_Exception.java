package CloudOfWar;
/**
* Cloud of War, API v0.2 
*/
public class CloudOfWar_Exception extends Exception
{ 
	int code;
	
	public CloudOfWar_Exception(String message, int code) 
	{
		super(message + "; Exception code: " + code);

		this.code = code;
	}


	public int getCode()
	{
		return this.code;
	}



	/**
	* Game doesn`t exist.
	* 
	* Raised when a function is called on a game before the game was created.
	*/
	public static final int GAME_DOES_NOT_EXIST=6;


	/**
	* Game has finished.
	* 
	* Game has finished. Most operations are unavailable for finished games.
	*/
	public static final int GAME_HAS_FINISHED=2;


	/**
	* Game in progress.
	* 
	* Some resources are unavailable and some actions not allowed for in progress games.
	*/
	public static final int GAME_IN_PROGRESS=1;


	/**
	* No opponent found.
	* 
	* This error is thrown when an opponent fails to connect to the game.
	*/
	public static final int OPPONENT_NOT_FOUND=7;


	/**
	* Player is not alive anymore.
	* 
	* Player is not alive so he can`t move anymore.
	*/
	public static final int PLAYER_NOT_ALIVE=3;


	/**
	* Wrong Parameter.
	* 
	* .
	*/
	public static final int WRONG_PARAMETER_GIVEN=8;


	/**
	 * SerialVersionUID is calculated from the API version but with all the '.' removed.
	 */
	private static final long serialVersionUID = 0;
}