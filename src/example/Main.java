package example;

import CloudOfWar.CloudOfWar;
import JSONRPC.filters.client.SignatureAdd;
import JSONRPC.filters.client.DebugLogger;
import JSONRPC.Client;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {
        String strEndpointURL;
        int nPlayerID = 186;//Should be changed!
        Boolean bPlanesAlive = true;
        
        strEndpointURL = "http://games.bigstep.local/api/cloud-of-war"; // Might need to be changed. Read the instructions provided.

        try {    
            
            CloudOfWar cl= new CloudOfWar(strEndpointURL);

            cl.addFilterPlugins(new DebugLogger(DebugLogger.LOG_TO_CONSOLE));
            cl.setHTTPCredentials("team4@bigstep.io", "team4pass");//Should be changed!
            
            HashMap<String, Object> game=cl.testmatch(nPlayerID);

            String color = (String)game.get("playerColor");
            String enemyColor;
                    
            if (color.equals("red")) {
                enemyColor = new String("blue");
            }
            else {
                enemyColor = new String("red");
            }
            
            int gameID = (int)game.get("gameID");

            List<Object> map = cl.terrainTiles(gameID);
            MyMap.instance().init(map.size());
            
            Map<String, Object> states;
            List<Object> moves = new ArrayList<Object>();
            
            states = cl.unitStates(nPlayerID, gameID, moves);
            
            //System.out.println(states.toString());            
            
            Player me = new Player(map.size());
            Enemy enemy = new Enemy();
            
            AI ai = new AI();
            
            me.setAI(ai);
            
            ai.setPlayer(me);
            
            ArrayList<Plane> myPlanes = new ArrayList<Plane>();
            ArrayList<Plane> enemyPlanes = new ArrayList<Plane>();
            ArrayList<Tornado> tornado = new ArrayList<Tornado>();
            
            while(bPlanesAlive)
            {
                MyMap.instance().reset();
              
                myPlanes.clear();
                enemyPlanes.clear();
                tornado.clear();
                
                moves = new ArrayList<Object>();
                ArrayList<Object> planes = (ArrayList)states.get(color);
                ArrayList<Object> enemies = (ArrayList)states.get(enemyColor);
                ArrayList<Object> tornados = (ArrayList)states.get("neutral");
                
                for (int i = 0; i < tornados.size(); i++) {
                    Tornado t = new Tornado();
                    HashMap<String,Object> tor = (HashMap<String,Object>)tornados.get(i);
                    t.setDx((String)tor.get("direction"));
                    t.setDy((String)tor.get("direction"));
                    t.setX((Integer)tor.get("tileX"));
                    t.setY((Integer)tor.get("tileY"));
                    MyMap.instance().set(t.getX(), t.getY(), "neutral");
                    tornado.add(t);
                }
                
                for (int i = 0; i < planes.size(); i++) {
                    Plane p = new Plane();
                    int x, y;
                    HashMap<String, Object> plane = (HashMap<String, Object>)planes.get(i);
                    p.setMapSize(map.size());
                    p.setX((Integer)plane.get("tileX"));
                    p.setY((Integer)plane.get("tileY"));
                    x = p.getX();
                    y = p.getY();
                    if (!MyMap.instance().get(x, y).equals("0")) {
                        p.setAlive(false);
                    }
                    p.setAltitude((Integer)plane.get("altitude"));
                    p.UpdateAmmo((Integer)plane.get("ammunition"));
                    p.setDx((String)plane.get("direction"));
                    p.setDy((String)plane.get("direction"));
                    p.setWeaponRange((Integer)plane.get("weaponRange"));
                    p.ID = (Integer)plane.get("playerUnitID");
                    myPlanes.add(p);
                }
                me.setPlanes(myPlanes);
                
                for(int i=0; i<enemies.size(); i++) {
                    Plane p = new Plane();
                    p.setMapSize(map.size());
                    HashMap<String,Object> plane = (HashMap<String,Object>)enemies.get(i);
                    p.setX((Integer)plane.get("tileX"));
                    p.setY((Integer)plane.get("tileY"));
                    int x, y;
                    x = p.getX();
                    y = p.getY();
                    if (!MyMap.instance().get(x, y).equals("0")) {
                        p.setAlive(false);
                    }
                    p.setAltitude((Integer)plane.get("altitude"));
                    p.setDx((String)plane.get("direction"));
                    p.setDy((String)plane.get("direction"));
                    p.UpdateAmmo((Integer)plane.get("ammunition"));
                    p.setWeaponRange((Integer)plane.get("weaponRange"));
                    p.ID = (Integer)plane.get("playerUnitID");
                    enemyPlanes.add(p);
                }
                enemy.setPlanes(enemyPlanes);
                
                ai.setTornado(tornado);
                ai.setEnemy(enemy);
                
                moves = new ArrayList<Object>();
                
                ArrayList<Pair<String, String>> mvs = new ArrayList<Pair<String, String>>();
                mvs = me.getMoves2();
                
               ArrayList<String> attackMoves = ai.getAttackMoves();
                
                /*for (int i=0; i < mvs.size(); i++ ) {
                    if (mvs.get(i) != null)
                        System.out.println(" mutari: " + mvs.get(i).first + " " + mvs.get(i).second);
                    else
                        System.out.println("NULL");
                }*/
                ArrayList<Boolean> wpns = new ArrayList<Boolean>();
                wpns = me.getWeaponStates();
                        
                for(int i=0; i<planes.size(); i++)
                {
                    if (!myPlanes.get(i).isAlive()) {
                        continue;
                    }
                    
                    HashMap<String,Object> plane = (HashMap<String,Object>)planes.get(i);
                
                    HashMap<String,Object> move = new HashMap<String, Object>();
                    move.put("playerUnitID", plane.get("playerUnitID"));
                    
                   boolean fire = false;
                    
                        if (myPlanes.get(i).getAmmo() != 0 && attackMoves.get(i) != null) {
                           move.put("direction", attackMoves.get(i));
                        }
                        else {
                            move.put("direction", mvs.get(i).first);
                        }
                    
                    //boolean fire = ai.shouldFire(myPlanes.get(i), mvs.get(i).first);
                    //}
                    //checking friendly fire
                        if (myPlanes.get(i).getAmmo() != 0 && attackMoves.get(i) != null) {
                            fire = true;
                            for (int j = 0; j < mvs.size(); j++) {
                                if (j == i)
                                    continue;
                                if (!myPlanes.get(j).isAlive())
                                    continue;
                                Point current;
                                if (mvs.get(i).first.equals("dive"))
                                    current = new Point(myPlanes.get(i).getX() + 2 * myPlanes.get(i).getDx(), 
                                                        myPlanes.get(i).getY() + 2 * myPlanes.get(i).getDy());
                                else
                                    current = myPlanes.get(i).getNextMove(mvs.get(i).first);
                                Pair<Integer, Integer> speeds = myPlanes.get(i).getNewSpeeds(mvs.get(i).first);
                                int dx = speeds.first;
                                int dy = speeds.second;
                                Point other = myPlanes.get(j).getNextMove(mvs.get(j).first);
                                for (int k = 0; k < myPlanes.get(i).getRange(); k++) {
                                    if (other.x == current.x + dx * k && other.y == current.y + dy * k)
                                        fire = false;
                                }
                            }
                        }
                    
                    
                    move.put("weapon", fire);
                    moves.add(i, move);
                }
                
                states = cl.unitStates(nPlayerID, gameID, moves);                 
                
                //System.out.println("state :");
                //System.out.println(states.toString());
                if (((ArrayList)states.get(color)).isEmpty())
                {
                    bPlanesAlive = false;
                }
            }
            
            Map<String, Object> replay = cl.replay(gameID, nPlayerID);

        } catch (java.io.IOException e) {
            System.out.println(e);
        }  
    }
}