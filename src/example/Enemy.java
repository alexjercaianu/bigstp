/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import java.util.ArrayList;

/**
 *
 * @author relu
 */
public class Enemy {
    private ArrayList<Plane> planes;
    public Enemy() {
        planes = new ArrayList<Plane>();
    }
    public Enemy(ArrayList<Plane> planes) {
        this.planes = planes;
    }
    
    public ArrayList<Plane> getPlanes() {
        return planes;
    }
    
    public void setPlanes(ArrayList<Plane> p) {
        planes = p;
    
    }
}
