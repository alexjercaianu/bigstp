/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

/**
 *
 * @author jerki
 */
public class Point {

    public int x;
    public int y;
    
    public Point() {
        x = 0;
        y = 0;
    }
    
    public Point(int a, int b) {
        x = a;
        y = b;
    }
    
    public boolean equals(Point other) {
        return other.x == x && other.y == y;
    }
}
