/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import java.util.ArrayList;

/**
 *
 * @author relu
 */
public class AI {
    private Player player; //= new Player();
    private Enemy enemy = new Enemy();
    private ArrayList<Tornado> tornados = new ArrayList<Tornado>();
    
    public AI() {
    
    }
    
    public AI(Enemy enemy, ArrayList<Tornado> tornados) {
        this.enemy = enemy;
        this.tornados = tornados;
    }
            
    public Pair<Boolean, String> CheckSafe(Point p)
    {
        if (!MyMap.instance().checkInBounds(p.x, p.y))
            return new Pair(false, "out of bounds");
        int i;
        ArrayList<Plane> enemy_planes;
        ArrayList<Pair<String, Point> > endangered_points = new ArrayList<Pair<String, Point> >();
        enemy_planes = enemy.getPlanes();
        
        //adding threatened positions
        for (i = 0; i < enemy_planes.size(); ++i) {
            if (enemy_planes.get(i) != null)
                if (enemy_planes.get(i).isAlive()) {
                    //System.out.println("mesaj!!:");
                    endangered_points.addAll(getAttackPoints(enemy_planes.get(i)));
                    //System.out.println("coordonate avioane inamice:" + enemy_planes.get(i).getX() + " " + enemy_planes.get(i).getY());
                }
        }
        endangered_points.addAll(possibleFutureEnemyPositions());
        
        /*adding positions of teammates - wrong, checking collision with teammates
            already done in Player.getmoves2()*/
        /*ArrayList<Plane> teammates;
        Point pos = new Point(0,0);
        teammates = player.getPlanes();
        for (i = 0; i < teammates.size(); i++) {
            pos.x = teammates.get(i).getX();
            pos.y = teammates.get(i).getY();
            endangered_points.add(new Pair<String,Point>("friend-position", pos));
        }*/
        
        //System.out.println("ATENTIE::");
        //System.out.println(endangered_points.size());

        /*for (i = 0; i < endangered_points.size(); ++i)
                System.out.println("punctele de pericol sunt: " + endangered_points.get(i).second.x + " " + endangered_points.get(i).second.y + " " + endangered_points.get(i).first);
*/

        for (i = 0; i < endangered_points.size(); i++) {
            if (endangered_points.get(i).second.x == p.x && 
                    endangered_points.get(i).second.y == p.y)
                return new Pair(false, "enemy-danger");
        }
        for (i = 0; i < tornados.size(); i++) {
            if (tornados.get(i).getX() + tornados.get(i).getDx() == p.x &&
                    tornados.get(i).getY() + tornados.get(i).getDy() == p.y)
                return new Pair(false, "tornado-danger");
        }
        return new Pair(true, "no threat");
    }
    
    public ArrayList<Pair<String, Point>> getAttackPoints(Plane p) {
        
        ArrayList<Pair<String, Point>> ret = new ArrayList<Pair<String, Point>>();
        int weaponRange = p.getRange();
        
        int x = p.getX();
        int y = p.getY();
        
        int dx = p.getDx();
        int dy = p.getDy();
        
        int fx = x + dx;
        int fy = y + dy;
        
        for (int i = 0; i < weaponRange; i++) {
            fx += dx;
            fy += dy;
            if (MyMap.instance().checkInBounds(fx, fy)) {
                ret.add(new Pair("forward", new Point(fx, fy)));
            }
        }
        
        int dashx = x + 2 * dx;
        int dashy = y + 2 * dy;
        
        dashx = dashx + weaponRange * dx;
        dashy = dashy + weaponRange * dy;
        if (MyMap.instance().checkInBounds(dashx, dashy)) {
            ret.add(new Pair("dive", new Point(dashx, dashy)));
        }
        
        int dxRight = dx;
        int dyRight = dy;
        if (dx == 0) {
            dyRight = 0;
            dxRight = dy;
        }
        else {
            dyRight = -dx;
            dxRight = 0; 
        }

        int xr = x + dxRight;
        int yr = y + dyRight;
        
        for (int i = 0; i < weaponRange; i++) {
            xr += dxRight;
            yr += dyRight;
            if (MyMap.instance().checkInBounds(xr, yr)) {
                ret.add(new Pair("right", new Point(xr, yr)));
            }
        }
        
        int dxLeft = dx;
        int dyLeft = dy;
        if (dx == 0) {
            dyLeft = 0;
            dxLeft = -dy;
        }
        else {
            dxLeft = 0;
            dyLeft = dx;
        }
        
        int xl = x + dxLeft;
        int yl = y + dyLeft;
        
        for (int i = 0; i < weaponRange; i++) {
            xl += dxLeft;
            yl += dyLeft;
            if (MyMap.instance().checkInBounds(xl, yl)) {
            ret.add(new Pair("left", new Point(xl, yl)));
            }
        }
        return ret;
    }
    
    public void setEnemy(Enemy other) {
        enemy = other;
    }
    
    public void setTornado(ArrayList<Tornado> other) {
        tornados = other;
    }
    
    public void setPlayer(Player p) {
        player = p;
    }
    
    ArrayList<Point> getFuturePositions(int ID) {
        ArrayList<Point> futurePos = new ArrayList<Point>();
        ArrayList<Plane> enemies = enemy.getPlanes();
        ArrayList<Plane> myPlanes = player.getPlanes();
        
        int X[] = {1, -1, 0, 0};
        int Y[] = {0, 0 , 1, -1};
        for (int i = 0; i < enemies.size(); i++) {
            Plane e = enemies.get(i);
            int x = e.getX();
            int y = e.getY();
            int newX = x;
            int newY = y;
            if (e.getDx() == 0) {
                for (int j = 0; j < 2; j++) {
                    if (MyMap.instance().checkInBounds(newX + X[j], newY))
                        futurePos.add(new Point(newX + X[j], newY));
                }
                if (MyMap.instance().checkInBounds(newX, newY + e.getDy()))
                    futurePos.add(new Point(newX, newY + e.getDy()));
            }
            else {
                for (int j = 0; j < 2; j++) {
                    if (MyMap.instance().checkInBounds(newX, newY + X[j]))
                        futurePos.add(new Point(newX, newY + X[j]));
                }
                if (MyMap.instance().checkInBounds(newX + e.getDx(), newY))
                    futurePos.add(new Point(newX + e.getDx(), newY));
            }
            
           /* for (int j = 0; j < 4; j++) {
                int newX = x + X[j];
                int newY = y + Y[j];*/
                
                
            }
        
        for (int i = 0; i < myPlanes.size(); i++) {
            if (ID == myPlanes.get(i).ID)
                continue;
            Plane e = myPlanes.get(i);
            int x = e.getX();
            int y = e.getY();
            int newX = x;
            int newY = y;
            if (e.getDx() == 0) {
                for (int j = 0; j < 2; j++) {
                    if (MyMap.instance().checkInBounds(newX + X[j], newY))
                        futurePos.add(new Point(newX + X[j], newY));
                }
                if (MyMap.instance().checkInBounds(newX, newY + e.getDy()))
                    futurePos.add(new Point(newX, newY + e.getDy()));
            }
            else {
                for (int j = 0; j < 2; j++) {
                    if (MyMap.instance().checkInBounds(newX, newY + X[j]))
                        futurePos.add(new Point(newX, newY + X[j]));
                }
                if (MyMap.instance().checkInBounds(newX + e.getDx(), newY))
                    futurePos.add(new Point(newX + e.getDx(), newY));
            }
        }
        
        return futurePos;
    }
    
    public boolean checkFutureOverlap(Plane plane, String move) {
        ArrayList<Point> pos = getFuturePositions(plane.ID);
        Point p = plane.getNextMove(move);
        for (int i = 0; i < pos.size(); i++) {
            if (pos.get(i).equals(p)) {
                return false;
            }
        }
        
        for (int i = 0; i < tornados.size(); i++) {
            Tornado t = tornados.get(i);
            if (p.equals(new Point(t.getX() + t.getDx(), t.getY() + t.getDy())))
                return false;
        }
        return true;
    }
    
    public ArrayList<String> getAttackMoves() {
        ArrayList<String> ret = new ArrayList<String>();
        ArrayList<Boolean> isAttacked = new ArrayList<Boolean>();
        ArrayList<ArrayList<Pair<String, Point>>> offensivePoints = player.getOffensivePoints();
        
        ArrayList<Plane> enemies = enemy.getPlanes();
        ArrayList<Plane> myPlanes = player.getPlanes();
        for (int i = 0; i < enemies.size(); i++) {
            isAttacked.add(false);
        }
        
        for (int i = 0; i < myPlanes.size(); i++) {
            ret.add(null);
        }
        for (int i = 0; i < offensivePoints.size(); i++) {
            if (!myPlanes.get(i).isAlive()) {
                continue;
            }
            
            ArrayList<Pair<String, Point>> current = offensivePoints.get(i);
            for (int j = 0; j < current.size(); j++) {
                for (int k = 0; k < enemies.size(); k++) {
                    if (enemies.get(k) != null && enemies.get(k).isAlive()) { 
                        Plane e = enemies.get(k);
                        Pair<String, Point> move = current.get(j);
                        Plane currentPlane = myPlanes.get(i);
                        if (move.second.x == e.getX() + e.getDx() &&
                                move.second.y == e.getY() + e.getDy() && 
                                !isAttacked.get(k) &&
                                checkFutureOverlap(currentPlane, move.first) 
                                //&& !e.facingPlane(currentPlane)) 
                                ){
                            isAttacked.set(k, true);
                            ret.set(i, current.get(j).first);
                            j = current.size();
                            k = enemies.size();
                        }
                    }
                }
            }
        }
        return ret;
    }
    
    public boolean shouldFire(Plane p, String move) {
        ArrayList<Plane> enemies = enemy.getPlanes();
        Point pos = p.getNextMove(move);
        ArrayList<Pair<String, Point>> possiblePos = possibleFutureEnemyPositions();
        
        int newX = pos.x;
        int newY = pos.y;
        Pair<Integer, Integer> speeds = p.getNewSpeeds(move);
        int newDx = speeds.first;
        int newDy = speeds.second;
        for (int i = 0; i < enemies.size(); i++) {
            int x1 = newX;
            int y1 = newY;
            Plane e = enemies.get(i);
            for (int k = 0; k < possiblePos.size(); k++) {
                Point px = possiblePos.get(k).second;
                for (int j = 0; j < p.getRange(); j++) {
                    x1 += newDx;
                    y1 += newDy;
                    if (px.x == x1 && px.y == y1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public ArrayList<Pair<String, Point>> possibleFutureEnemyPositions() {
        ArrayList<Pair<String, Point>> pos = new ArrayList<Pair<String, Point>>();
        ArrayList<Plane> enemies = enemy.getPlanes();
        for (int i = 0; i < enemies.size(); i++) {
            pos.add(new Pair("left", enemies.get(i).getNextMove("left")));
            pos.add(new Pair("forward", enemies.get(i).getNextMove("forward")));
            pos.add(new Pair("right", enemies.get(i).getNextMove("right")));
        }
        return pos;
    }
}
