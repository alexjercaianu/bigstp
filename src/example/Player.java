/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import java.util.ArrayList;

/**
 *
 * @author jerki&relu
 */
public class Player {
    private ArrayList<Plane> planes;
    private boolean weapon;
    private Enemy enemy;
    private ArrayList<Tornado> tornados;
    private final int MAPSIZE;
    private AI ai;
    public Player(int mapsize) {
        MAPSIZE = mapsize;
        planes = new ArrayList<Plane>();
        ai = new AI();
    }
    
    public Player(ArrayList<Plane> planes, Enemy enemy, 
            ArrayList<Tornado> tornados, AI ai, int mapsize) {
        this.enemy = enemy;
        this.planes = planes;
        this.tornados = tornados;
        this.MAPSIZE = mapsize;
        this.ai = ai;
    }
            
    public void setPlanes(ArrayList<Plane> p) {
        planes = p;
    }
    public ArrayList<Pair<String, Point> >getSafeMoves(Plane p) {
        
        //priority over safe moves
        
        ArrayList<Pair<String, Point> > resultlist = new ArrayList<Pair<String, Point> >();
        if (p.isAlive()) {
            Point pos = new Point();
            pos.x = p.getX();
            pos.y = p.getY();
            if (ai.CheckSafe(p.goForward(pos)).first)
                resultlist.add(new Pair<String, Point>("forward", p.goForward(pos)));
            if (ai.CheckSafe(p.goRight(pos)).first)
                resultlist.add(new Pair<String, Point>("right", p.goRight(pos)));
            if (ai.CheckSafe(p.goLeft(pos)).first)
                resultlist.add(new Pair<String, Point>("left", p.goLeft(pos)));
            return resultlist;
        }
        return resultlist;
    }
    public ArrayList<Pair<String, Point> >getDashMoves(ArrayList<Plane> planes) {
        ArrayList<Pair<String, Point> > resultlist = new ArrayList<Pair<String,Point> >();
        int i;
        for (i = 0; i < planes.size(); ++i) {
            if (planes.get(i).isAlive()) {
                if (planes.get(i).getAltitude() > planes.get(i).getDashLoss() &&
                        ai.CheckSafe(planes.get(i).goDive(planes.get(i).getPosition())).first) {
                    resultlist.add(new Pair<String, Point>
                            ("dive", planes.get(i).goDive(planes.get(i).getPosition())));
                }
                else
                    resultlist.add(null);
            }
            else
                resultlist.add(null);
        }
        return resultlist;
    }
    public ArrayList<Pair<String, Point> > getThreatenedMoves(Plane p) {
        
        //to-do priority: enemy position, simultaneous kill
        
        ArrayList<Pair<String, Point> > resultlist = new ArrayList<Pair<String,Point> >();
        int i;
        if (p.isAlive()) {
            if (ai.CheckSafe(p.goForward(p.getPosition())).first == false &&
                   ai.CheckSafe(p.goForward(p.getPosition())).second.equals("enemy-danger") )
                resultlist.add(new Pair<String,Point>("forward", p.goForward(p.getPosition())));
            if (ai.CheckSafe(p.goRight(p.getPosition())).first == false &&
                   ai.CheckSafe(p.goRight(p.getPosition())).second.equals("enemy-danger") )
                resultlist.add(new Pair<String,Point>("right", p.goRight(p.getPosition())));
            if (ai.CheckSafe(p.goLeft(p.getPosition())).first == false &&
                   ai.CheckSafe(p.goLeft(p.getPosition())).second.equals("enemy-danger") )
                resultlist.add(new Pair<String,Point>("left", p.goLeft(p.getPosition())));
            if (ai.CheckSafe(p.goDive(p.getPosition())).first == false && 
                    ai.CheckSafe(p.goDive(p.getPosition())).second.equals("enemy-danger")) 
                resultlist.add(new Pair<String, Point>("dive", p.goDive(p.getPosition())));
        }
        
        //find kamikaze option
        ArrayList<Pair<String, Point> > enemypos;
        enemypos = ai.possibleFutureEnemyPositions();
        Pair<String, Point> aux;
        for (i = 0; i < enemypos.size(); ++i) {
            for (int k = 0; k < resultlist.size(); ++k)
                if (resultlist.get(k).second.x == enemypos.get(i).second.x &&
                      resultlist.get(k).second.y == enemypos.get(i).second.y  ) {
                    aux = resultlist.get(0);
                    resultlist.set(0, resultlist.get(k));
                    resultlist.set(k,aux);
                }
                    
        }
        
        return resultlist;
    }
    public ArrayList<Pair<String, String> > getMoves2() {
        
        ArrayList<Pair<String, String> > resultlist = new ArrayList<Pair<String, String> >();
        ArrayList<Pair<String, Point> > dashmoves = getDashMoves(planes);
        ArrayList<Point> futurepositions = new ArrayList<Point>();
                
        for (int i = 0; i < planes.size(); ++i) {
            if (!planes.get(i).isAlive()) {
                continue;
            }
            ArrayList<Pair<String, Point> > safemoves = getSafeMoves(planes.get(i));
            ArrayList<Pair<String, Point> > csafemoves = new ArrayList<Pair<String, Point> >(getSafeMoves(planes.get(i)));
            ArrayList<Pair<String, Point> > threatmoves = getThreatenedMoves(planes.get(i));
            ArrayList<Pair<String, Point> > cthreatmoves =  new ArrayList<Pair<String, Point> > (getThreatenedMoves(planes.get(i)));
            if (safemoves != null && safemoves.isEmpty() == false && safemoves.size() > 0) {
                for (int k = 0; k < safemoves.size(); ++k) {
                    for (int j = 0; j < futurepositions.size(); ++j) {
                        if (safemoves.get(k).second.x == futurepositions.get(j).x &&
                                safemoves.get(k).second.y == futurepositions.get(j).y) {
                            
                            if (csafemoves.size() >= k)
                                csafemoves.remove(k);
                            break;
                        }
                    }
                }
                safemoves = csafemoves;
                int it = 0;
                while (it < safemoves.size()) {
                    if (safemoves.get(it).second.x == 0 && safemoves.get(it).second.y == 0) {
                        it++;
                        continue;
                    }
                    if( safemoves.get(it).second.x == MAPSIZE-1 && safemoves.get(it).second.y == MAPSIZE-1){
                        it++;
                        continue;
                    }
                    if( safemoves.get(it).second.x == 0 && safemoves.get(it).second.y == MAPSIZE-1){
                        it++;
                        continue;
                    }
                    if( safemoves.get(it).second.x == MAPSIZE-1 && safemoves.get(it).second.y == 0){
                        it++;
                        continue;
                    }
                    break;
                }
                if (safemoves.size() > 0) {
                    if (it == safemoves.size() ) {
                        safemoves.get(0);
                        futurepositions.add(safemoves.get(0).second);
                        resultlist.add(new Pair<String,String>(safemoves.get(0).first, "safe-move"));
                        continue;
                    }
                    else {
                        //System.out.println("CE PLM NU MERGE COLTU " + safemoves.get(it).second.x + " " + safemoves.get(it).second.y );
                        futurepositions.add(safemoves.get(it).second);
                        resultlist.add(new Pair<String,String>(safemoves.get(it).first, "safe-move"));
                        continue;
                    }

                }
            }
            boolean candash = true;
            if (dashmoves != null)
                if (dashmoves.get(i) != null) {
                    for (int k = 0; k < futurepositions.size(); ++k) {
                        if (dashmoves.get(i).second.x == futurepositions.get(k).x &&
                                dashmoves.get(i).second.y == futurepositions.get(k).y)
                            candash = false;
                    }
                    if (candash) {
                        futurepositions.add(dashmoves.get(i).second);
                        resultlist.add(new Pair<String,String>(dashmoves.get(i).first, "safe-dash"));
                        continue;
                    }
                }   
            if (threatmoves != null) {
                for (int j = 0; j < threatmoves.size(); ++j)
                    for (int k = 0; k < futurepositions.size(); ++k)
                        if (threatmoves.get(j).second.x == futurepositions.get(k).x &&
                              threatmoves.get(j).second.y == futurepositions.get(k).y ) {
                            if (cthreatmoves.size() >= j)
                                cthreatmoves.remove(j);
                            break;
                        }
                threatmoves = cthreatmoves;
                if (threatmoves.isEmpty() == false) {
                    futurepositions.add(threatmoves.get(0).second);
                    resultlist.add(new Pair<String, String>(threatmoves.get(0).first, "threat-move"));
                    continue;
                }
            }
            //absolut nicio sansa pentru avionul curent
            resultlist.add(new Pair<String, String>("forward", "no-chance")); 
        }
           
        return resultlist;
    }
    public ArrayList<Pair<String, String> > getMoves() {
        
        int i;
        Point p = new Point(0,0);
        ArrayList<Pair<String, String> > resultlist = new ArrayList<Pair<String, String> >();
        
        Pair<Boolean, String> resultF; 
        Pair<Boolean, String> resultR; 
        Pair<Boolean, String> resultL; 
        
        for (i = 0; i < planes.size(); i++) {
            if (planes.get(i).isAlive()) {
                
                // p - pozitia curenta a avionului i
                p.x = planes.get(i).getX();
                p.y = planes.get(i).getY();
                
                //priority : offense over defense;
                //           forward > right > left > dash-dead
                
                resultF = ai.CheckSafe(planes.get(i).goForward(p));
                System.out.println("motiv forward:" + resultF.first + " " + resultF.second);
                if (resultF.first) {
                    resultlist.add(new Pair<String,String>("forward", "no threat"));
                    continue;
                }
                resultR = ai.CheckSafe(planes.get(i).goRight(p));
                if (resultR.first) {
                    resultlist.add(new Pair<String,String>("right", "no threat"));
                    continue;
                }
                resultL = ai.CheckSafe(planes.get(i).goLeft(p));
                if (resultL.first) {
                    resultlist.add(new Pair<String,String>("left", "no threat"));
                    continue;
                }
                if (resultF.second.equals("dive")) {
                    resultlist.add(new Pair<String, String>("forward", "dash-dead"));
                    continue;
                }
                if (resultR.second.equals("dive")) {
                    resultlist.add(new Pair<String, String>("right", "dash-dead"));
                    continue;
                }
                if (resultL.second.equals("dive")) {
                    resultlist.add(new Pair<String, String>("left", "dash-dead"));
                    continue;
                }
                if (resultF.second.equals("out of bounds")) {
                    if (resultR.second.equals("out of bounds")) {
                         resultlist.add(new Pair<String, String>("left", "no choice, cornered"));
                         continue;
                    }
                    else {
                        resultlist.add(new Pair<String, String>("right", "front cornered"));
                        System.out.println("se misca la dreapta");
                        continue;
                    }
                }
                else {
                    resultlist.add(new Pair<String, String>("forward", "forward is not out of bounds"));
                    continue;
                }
                
            
               }
            else
                resultlist.add(null);
        }
        return resultlist;
    }
    
    public ArrayList<Boolean> getWeaponStates()
    {
        ArrayList<Boolean> resultlist = new ArrayList<Boolean>();
        int i;
        for (i = 0; i < planes.size(); ++i) {
            if (planes.get(i).isAlive()) {
                resultlist.add(planes.get(i).getWeaponState());
            }
            else
                resultlist.add(null);
        }
        return resultlist;
    }
    
    public ArrayList<ArrayList<Pair<String, Point>>> getOffensivePoints() 
    {
        ArrayList<ArrayList<Pair<String, Point>>> offensivePoints = new ArrayList<ArrayList<Pair<String, Point>>>();
        for (int i = 0; i < planes.size(); i++) {
            offensivePoints.add(null);
        }
        for (int i = 0; i < planes.size(); i++) {
            Plane p = planes.get(i);
            offensivePoints.set(i, ai.getAttackPoints(p));
        }
        return offensivePoints;
    }
    
    public void setAI(AI other) {
        ai = other;
    }
    
    public ArrayList<Plane> getPlanes() {
        return planes;
    }
}
