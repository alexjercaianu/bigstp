/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 *
 * @author relu
 */
public class Tornado {
    
    private int dx;
    private int dy;
    private int x;
    private int y;
    private final int MAPSIZE;
    private boolean alive;

    public Tornado() {
        MAPSIZE = 10;
    }
    public void update()
    {
        if (checkInBounds())
            move();
        else
            alive = false;
    }
    public int move()
    {
        if (alive == true) {
                if (dx * dy != 0 || (dx == 0 && dy == 0)) {
                System.out.println("illegal directions");
                return -1;
            }
                    
            if (dx != 0) {
                x += dx;
                return 0;
            }
            if (dy != 0) {
                
                y += dy;
                return 1;
            }
        }
        return -1;
    }
    
    public boolean checkInBounds() {
        if (x >= MAPSIZE || x < 0)
            return false;
        if (y >= MAPSIZE || y < 0)
            return false;
        return true;
    }
    public int getX() {
        return x;
        
    }
    public int getY() {
        return y;
    }
   
    public boolean isAlive() {
        if (alive)
            return true;
        else
            return false;
    }
            
    public Tornado (int dx, int dy, int x, int y, int MAPSIZE) 
    {
        this.dx = dx;
        this.dy = dy;
        this.x = x;
        this.y = y;
        this.MAPSIZE = MAPSIZE;
        alive = true;
    }
    public int getDx() {
        return dx;
    }
    
    public int getDy() {
        return dy;
    }
    
    public void setDx(String dir) {
        if (dir.equals("north"))
            dx = -1;
        else if (dir.equals("south"))
            dx = 1;
        else dx = 0;
    }
    
    public void setDy(String dir) {
        if (dir.equals("west"))
            dy = -1;
        else if (dir.equals("east"))
            dy = 1;
        else dy = 0;
    }
    
    public void setX(int value) {
        x = value;
    }
    
    public void setY(int value) {
        y = value;
    }
}
