/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import java.util.HashMap;

/**
 *
 * @author jerki
 */
public class Plane {
    
    public int ID;
    private int ammo;
    private int altitude;
    private boolean weapon;
    private int x;
    private int y;
    private int dx;
    private int dy;
    private boolean alive = true;
    private int mapSize = 10;
    private int maxAltitude = 13;
    private int AmmoRefillTurns = 3;
    private int AmmoRefillSize = 5;
    private int turnswithoutammo = 0;
    private String last_move;
    private final int dash_loss = 4;


    private Point position = new Point();

    private int weaponRange;
    public void setammoRefill(int value) {
        AmmoRefillTurns = value;
    } 
    public Plane() {
        weapon = false;
        ammo = 5;
        alive = true;
        last_move = "none";
      
    }
    public boolean isAlive (){
        return alive;
    }
    public int getAmmo() {
        return ammo;
    }
    public int getDashLoss() {
        return dash_loss;
    }
    public int getAltitude() {
        return altitude;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public void decrementAltitude(int units) {
        altitude -= units;
        if (altitude <= 0)
            alive = false;
    }
    
    public void incrementAltitude(int units) {
        altitude += units;
        if (altitude >= maxAltitude)
            altitude = maxAltitude;
    }
    
    public void decrementAmmo() {
        if (ammo > 0)
            ammo--;
    }
    
    public boolean checkInBounds() {
        if (x >= mapSize || x < 0) {
            alive = false;
            return false;
        }
        if (y >= mapSize || y < 0) {
            alive = false;
            return false;
        }
        return true;
    }
    
    public void setX(int value) {
        position.x = value;
        x = value;
    }
    
    public void setY(int value) {
        position.y = value;
        y = value;
    }
    
    public void setAltitude(int value) {
        altitude = value;
    }
    
    public void setDx(String dir) {
        if (dir.equals("north"))
            dx = -1;
        else if (dir.equals("south"))
            dx = 1;
        else dx = 0;
    }
    
    public void setDy(String dir) {
        if (dir.equals("west"))
            dy = -1;
        else if (dir.equals("east"))
            dy = 1;
        else dy = 0;
    }
    
    public int getDx() {
        return dx;
    }
    
    public int getDy() {
        return dy;
    }
    public Point getPosition() {
        return position;
    }
    public Point goRight(Point p)
    {
        if (dy == 1) 
            return new Point(p.x+1, p.y);
        if (dy == -1)
            return new Point(p.x-1, p.y);
        if (dx == 1)
            return new Point(p.x, p.y-1);
        if (dx == -1)
            return new Point(p.x, p.y+1);
       
        return null;
    }
    public int moveRight(int x, int y) {
        
        last_move = "left";
        decrementAltitude(2);
        if (dy == 1) {
           dy = 0;
           x--;
           dx = -1;
           return 1;
        }
        if (dy == -1) {
           dy = 0;
           x++;
           dx = 1;
           return 2;
        }
        if (dx == 1) {
           dx = 0;
           y++;
           dy = 1;
           return -1;
        }
        if (dx == -1) {
           dx = 0;
           y--;
           dy = -1;
           return -2;
        }
        
        return -3;
        
    }
  
    public Point goLeft(Point p)
    {
        if (dy == 1) 
            return new Point(p.x-1, p.y);
        if (dy == -1)
            return new Point(p.x+1, p.y);
        if (dx == 1)
            return new Point(p.x, p.y+1);
        if (dx == -1)
            return new Point(p.x, p.y-1);
        return null;
    }
    
    public int moveLeft() {
        
            last_move = "left";
            decrementAltitude(2);
            if (dy == 1) {
                dy = 0;
                x++;
                dx = 1;
                return 1;
            }
            if (dy == -1) {
                dy = 0;
                x--;
                dx = -1;
                return 2;
            }
            if (dx == 1) {
                dx = 0;
                y--;
                dy = -1;
                return -1;
            }
            if (dx == -1) {
                dx = 0;
                y++;
                dy = 1;
                return -2;
            }
        
        return -3;
            
    }
    
    public Point goForward(Point p)
    {
        return new Point(p.x + dx, p.y + dy);
        
    }
    
    public int moveForward() {
       
        incrementAltitude(1);
        y += dy;
        x += dx;
        last_move = "forward";
        return 1;
        
        
    }
    
   
    public Point goDive(Point p) {
        return new Point(p.x + 2*dx, p.y + 2*dy);
    }
    
    public int moveDive() {
        x += 2*dx;
        y += 2*dy;
        decrementAltitude(4);
        last_move = "dive";
        return 0;
    }
    
    public String getLastMove() {
        return last_move;
    }
    
    public void setAlive(boolean value) {
        alive = value;

    }
    
    public void Fire() {
        if (ammo > 0)
            weapon = true;
    }
    public void UpdateAmmo(int value) {
        ammo = value;
    }
   
    public boolean getWeaponState() {
        return weapon;
    }
    
    public void setMaxAltitude(int value) {
        maxAltitude = value;
    }
    
    public int getRange() {
        return weaponRange;
    }
    
    public void setWeaponRange(int value) {
        weaponRange = value;
    }
    
    public Point getNextMove(String move) {
        if (move.equals("left")) {
            return goLeft(new Point(x, y));
        }
        else if (move.equals("right")) {
            return goRight(new Point(x, y));
        }
        else if (move.equals("forward")) {
            return goForward(new Point(x, y));
        }
        else 
            return goDive(new Point(x, y));
    }
    
    public boolean facingPlane(Plane p) {
        if (p.getDx() + dx == 0 && p.getDy() + dy == 0) {
            System.out.println(p.ID + " facing " + ID);
            return true;
        }
        return false;
    }
    
    Pair<Integer, Integer> getNewSpeeds(String move) {
        if (move.equals("forward")) {
            return new Pair(dx, dy);
        }
        else if (move.equals("dive")) {
            return new Pair(dx, dy);
        }
        else if (move.equals("left")) {
            if (dx == 0) {
                return new Pair(-dy, 0);
            }
            else {
                return new Pair(0, dx);
            }
        }
        else {
            if (dx == 0) {
                return new Pair(dy, 0);
            }
            else return new Pair(0, -dx);
        }
    }
    
    public void setMapSize(int value) {
        mapSize = value;
    }
}
