/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

/**
 *
 * @author jerki
 */
public class MyMap {
    private static MyMap instance = null;
    private int size;
    private String[][] map;
    
    //Singleton
    protected MyMap() 
    {
    }
    
    public static MyMap instance() 
    {
        if (instance == null)
            instance = new MyMap();
        return instance;
    }
    
    void init(int newSize)
    {
        size = newSize;
        map = new String[size][size];
    }
    
    public String get(int x, int y)
    {
        return map[x][y];
    }
    
    public void set(int x, int y, String value)
    {
        map[x][y] = value;
    }
    
    public void reset() 
    {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                map[i][j] = "0";
            }
        }
    }
    
    public boolean checkInBounds(int x, int y) 
    {
        if (x < 0 || x >= size)
            return false;
        if (y < 0 || y >= size)
            return false;
        return true;
    }
}