/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

/**
 *
 * @author jerki
 */
public class Pair<K, V> {
    public K first;
    public V second;
    
    Pair(K k, V v) {
        first = k;
        second = v;
    }

    Pair() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
